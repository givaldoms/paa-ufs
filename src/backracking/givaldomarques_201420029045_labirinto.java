package backracking;

import java.io.*;

public class givaldomarques_201420029045_labirinto {

    public static void imprimirMapa(char[][] mapa) {
        for (char[] aMapa : mapa) {
            for (char anAMapa : aMapa) {
                System.out.print(anAMapa + " ");
            }
            System.out.println();
        }

        System.out.println("==============================");
    }

    public static void main(String[] args) throws IOException {

        String nomeArq = args.length > 1 ? args[0] : "C:\\Users\\Givaldo Marques\\IdeaProjects\\projeto01-paa\\src\\entrada-backtracking.txt";
        String nomeSaida = args.length > 1 ? args[1] : "C:\\Users\\Givaldo Marques\\IdeaProjects\\projeto01-paa\\src\\saida-backtracking.txt";

        BufferedReader br = new BufferedReader(new FileReader(nomeArq));

        Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(nomeSaida), "utf-8"));
        int nl = Integer.parseInt(br.readLine());//número de linhas

        for (int i1 = 0; i1 < nl; i1++) {
            writer.append("L").append(String.valueOf(i1)).append(":\n");

            String[] s = br.readLine().split(" ");// [LARGURA] X [ALTURA]

            int l = Integer.parseInt(s[0]);//largura
            int a = Integer.parseInt(s[1]);//altura

            int[] pI = {0, 0};//posicao inicial

            char[][] mapa = new char[a][l];

            for (int j = 0; j < a; j++) {
                String[] s1 = br.readLine().split(" ");
                for (int k = 0; k < s1.length; k++) {
                    char c = s1[k].charAt(0);
                    if (c == 'X') {
                        pI[0] = j;
                        pI[1] = k;
                    }
                    mapa[j][k] = c;
                }

            }

            int[] pAi = new int[l * a];
            int[] pAj = new int[l * a];

            int _ai = 0;
            int _aj = 0;

            int i = pI[0];//linha
            int j = pI[1];//coluna

            pAi[_ai] = i;
            pAj[_aj] = j;

            writer.append("INICIO [").append(String.valueOf(i)).append(",").append(String.valueOf(j)).append("]").append("\n");
            while (true) {
                mapa[i][j] = 'p';
                _ai++;
                _aj++;

                if (i >= mapa.length - 1 || j >= mapa[0].length - 1 || i <= 0 || j <= 0) {
                    writer.append("SAIDA [").append(String.valueOf(i)).append(",").append(String.valueOf(j)).append("]\n");
                    break;
                }

                //direita
                if (mapa[i][j + 1] == '0') {
                    writer.append("D").append(" [").append(String.valueOf(i)).append(",").append(String.valueOf(j)).append("]->");
                    pAi[_ai] = i;
                    pAj[_aj] = j++;
                    mapa[i][j] = 'X';
                    writer.append("[").append(String.valueOf(i)).append(",").append(String.valueOf(j)).append("]\n");
                    continue;
                }

                //frente
                if (mapa[i - 1][j] == '0') {
                    writer.append("F").append(" [").append(String.valueOf(i)).append(",").append(String.valueOf(j)).append("]->");
                    pAi[_ai] = i--;
                    pAj[_aj] = j;
                    mapa[i][j] = 'X';
                    writer.append("[").append(String.valueOf(i)).append(",").append(String.valueOf(j)).append("]\n");
                    continue;
                }

                //esquerda
                if (mapa[i][j - 1] == '0') {
                    writer.append("E").append(" [").append(String.valueOf(i)).append(",").append(String.valueOf(j)).append("]->");
                    pAi[_ai] = i;
                    pAj[_aj] = j--;
                    mapa[i][j] = 'X';
                    writer.append("[").append(String.valueOf(i)).append(",").append(String.valueOf(j)).append("]\n");
                    continue;
                }

                //tras
                if (mapa[i + 1][j] == '0') {
                    writer.append("T").append(" [").append(String.valueOf(i)).append(",").append(String.valueOf(j)).append("]->");
                    pAi[_ai] = i++;
                    pAj[_aj] = j;
                    mapa[i][j] = 'X';
                    writer.append("[").append(String.valueOf(i)).append(",").append(String.valueOf(j)).append("]\n");
                    continue;
                }

                _ai--;
                _aj--;

                if ((_ai <= 0) || (_aj <= 0)) {
                    writer.append("SEM SAIDA").append("\n");
                    break;
                }

                //givaldomarques_201420029045_labirinto
                int aux1 = i;
                int aux2 = j;
                i = pAi[_ai--];
                j = pAj[_aj--];
                writer.append("BT [").append(String.valueOf(i)).append(",").append(String.valueOf(j)).append("]<-");
                writer.append("[").append(String.valueOf(aux1)).append(",").append(String.valueOf(aux2)).append("]\n");

            }

        }


        writer.close();

    }


}
