package compressao;

import java.io.*;
import java.text.DecimalFormat;

interface PriorityQueue {

    Position insert(Comparable x);

    Comparable findMin();

    Comparable deleteMin();

    boolean isEmpty();

    void makeEmpty();

    int size();

    void decreaseKey(Position p, Comparable newVal);

    interface Position {
        Comparable getValue();
    }
}

class CountingSort {
    void sort(Node arr[]) {
        int n = arr.length;

        Node output[] = new Node[n];
        int count[] = new int[256];
        for (int i = 0; i < 256; ++i)
            count[i] = 0;
        for (Node anArr : arr) ++count[anArr.valor];
        for (int i = 1; i <= 255; ++i)
            count[i] += count[i - 1];

        for (Node anArr : arr) {
            output[count[anArr.valor] - 1] = anArr;
            --count[anArr.valor];
        }
        System.arraycopy(output, 0, arr, 0, n);
    }

    void sort(int arr[]) {
        int n = arr.length;

        int output[] = new int[n];
        int count[] = new int[256];
        for (int i = 0; i < 256; ++i)
            count[i] = 0;
        for (int anArr : arr) ++count[anArr];
        for (int i = 1; i <= 255; ++i)
            count[i] += count[i - 1];

        for (int anArr : arr) {
            output[count[anArr] - 1] = anArr;
            --count[anArr];
        }
        System.arraycopy(output, 0, arr, 0, n);
    }
}

class Node implements Comparable {
    int valor;
    int prio;
    String comp;
    Node left = null;
    Node right = null;

    Node(int valor, int prio) {
        this.valor = valor;
        this.prio = prio;
    }


    @Override
    public int compareTo(Object o1) {
        Node o = (Node) o1;

        if (this.prio >= o.prio) {
            return 1;
        }

        return -1;
    }
}

class BinaryHeap implements PriorityQueue {
    private static final int DEFAULT_CAPACITY = 100;
    private int currentSize;      // Number of elements in heap
    private Comparable[] array; // The heap array


    BinaryHeap(Comparable[] items) {
        currentSize = items.length;
        array = new Comparable[items.length + 1];

        System.arraycopy(items, 0, array, 1, items.length);
        buildHeap();
    }


    public PriorityQueue.Position insert(Comparable x) {
        if (currentSize + 1 == array.length)
            doubleArray();

        // Percolate up
        int hole = ++currentSize;
        array[0] = x;

        for (; x.compareTo(array[hole / 2]) < 0; hole /= 2)
            array[hole] = array[hole / 2];
        array[hole] = x;

        return null;
    }


    public void decreaseKey(PriorityQueue.Position p, Comparable newVal) {
        throw new UnsupportedOperationException("Cannot use decreaseKey for binary heap");
    }


    public Comparable findMin() {
        if (isEmpty())
            throw new UnderflowException("Empty binary heap");
        return array[1];
    }


    public Comparable deleteMin() {
        Comparable minItem = findMin();
        array[1] = array[currentSize--];
        percolateDown(1);

        return minItem;
    }


    private void buildHeap() {
        for (int i = currentSize / 2; i > 0; i--)
            percolateDown(i);
    }


    public boolean isEmpty() {
        return currentSize == 0;
    }

    public int size() {
        return currentSize;
    }


    public void makeEmpty() {
        currentSize = 0;
    }


    private void percolateDown(int hole) {
        int child;
        Comparable tmp = array[hole];

        for (; hole * 2 <= currentSize; hole = child) {
            child = hole * 2;
            if (child != currentSize &&
                    array[child + 1].compareTo(array[child]) < 0)
                child++;
            if (array[child].compareTo(tmp) < 0)
                array[hole] = array[child];
            else
                break;
        }
        array[hole] = tmp;
    }


    private void doubleArray() {
        Comparable[] newArray;

        newArray = new Comparable[array.length * 2];
        System.arraycopy(array, 0, newArray, 0, array.length);
        array = newArray;
    }
}

class UnderflowException extends RuntimeException {

    UnderflowException(String message) {
        super(message);
    }
}

class RunLengthEncode {

    private int calcRest(int x) {
        return Math.abs((x % 8) - 8);
    }

    String encode(int[] source) {
        StringBuilder dest = new StringBuilder();
        for (int i = 0; i < source.length; i++) {
            int runLength = 1;
            while (i + 1 < source.length && source[i] == source[i + 1]) {
                runLength++;
                i++;
            }

            StringBuilder aux = new StringBuilder();
            for (int j = Integer.toBinaryString(runLength).length(); j < 8; j++) {
                aux.append("0");
            }

            dest.append(aux).append(Integer.toBinaryString(runLength)).append(Integer.toBinaryString(source[i]));

        }
        return dest.toString();
    }

}

class Huffman {

    BinaryHeap h;
    private StringBuilder p;

    Huffman() {
        p = new StringBuilder();
    }

    private int calcRest(int x) {
        return Math.abs((x % 8) - 8);
    }

    Node construirArvore() {
        while (h.size() > 1) {
            Node right = (Node) h.deleteMin();
            Node left = (Node) h.deleteMin();
            Node item = new Node(-1, right.prio + left.prio);
            item.left = left;
            item.right = right;
            h.insert(item);
        }

        return (Node) h.deleteMin();
    }

    void percorrer(Node node) {
        if (node == null) {
            p = new StringBuilder();
            return;
        }

        if (node.left == null && node.right == null) {//encontrou o valor
            if (p.length() == 0) {
                node.comp = "0";
            } else {
                node.comp = p.toString();
            }
            //System.out.println(node.valor + ": " + node.comp);
            return;
        }

        p.append("0");
        percorrer(node.right);
        p.deleteCharAt(p.length() - 1);

        p.append("1");
        percorrer(node.left);
        p.deleteCharAt(p.length() - 1);

    }

    private String binarySearch(Node[] c, int chave) {
        int tam = c.length;
        int inicio = 0, fim = tam - 1, meio;

        while (inicio <= fim) {
            meio = (fim + inicio) / 2;
            if (c[meio].valor == chave) {
                return c[meio].comp;
            }
            if (c[meio].valor > chave) {
                fim = meio - 1;
            } else {
                inicio = meio + 1;
            }
        }

        return null;
    }

    String encode(Node[] nodes, int[] input) throws IOException {
        StringBuilder sb = new StringBuilder();
        CountingSort cs = new CountingSort();
        cs.sort(nodes);

        for (int i : input) {
            String aux = binarySearch(nodes, i);
            sb.append(aux);
        }

        int n = calcRest(sb.length());

        for (int i = 0; i < n; i++) {
            sb.append("0");
        }

        return sb.toString();
    }

}

public class givaldomarques_201420029045_compressao {

    private static String binaryToHex(String bin) {
        switch (bin) {
            case "0000":
                return "0";
            case "0001":
                return "1";
            case "0010":
                return "2";
            case "0011":
                return "3";
            case "0100":
                return "4";
            case "0101":
                return "5";
            case "0110":
                return "6";
            case "0111":
                return "7";
            case "1000":
                return "8";
            case "1001":
                return "9";
            case "1010":
                return "A";
            case "1011":
                return "B";
            case "1100":
                return "C";
            case "1101":
                return "D";
            case "1110":
                return "E";
            case "1111":
                return "F";
            default:
                return null;
        }

    }


    public static void main(String[] args) throws IOException {

        RunLengthEncode rle = new RunLengthEncode();
        Huffman huffman = new Huffman();

        double millis = System.currentTimeMillis();
        StringBuilder arq = new StringBuilder();
//        String nomeArq = args.length > 1 ? args[0] : "C:\\Users\\Givaldo Marques\\Documents\\compressao.input.txt";
         String nomeArq = args.length > 1 ? args[0] : "C:\\Users\\Givaldo Marques\\IdeaProjects\\projeto01-paa\\src\\compressao\\entrada-compressao.txt";

        BufferedReader br = new BufferedReader(new FileReader(nomeArq));

        String nomeSaida = args.length > 1 ? args[1] : "C:\\Users\\Givaldo Marques\\IdeaProjects\\projeto01-paa\\src\\saida-compressao.txt";
        Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(nomeSaida), "utf-8"));

        int contSaida = -1;
        int quantidadeSequencias = Integer.parseInt(br.readLine());

        for (int i = 0; i < quantidadeSequencias; i++) {

            String[] line = br.readLine().split(" ");

            int t = Integer.parseInt(line[0]);
            int b[] = new int[t];

            for (int j = 0; j < t; j++) {
//                b[j] = Integer.parseInt(line[j + 1]);
                b[j] = Integer.parseInt(line[j + 1].substring(2, line[j + 1].length()), 16);
            }

            Node[] nodes = getValores(b);

            //========================================HUFFMAN===============================================
            huffman.h = new BinaryHeap(nodes);
            Node tree = huffman.construirArvore();
            huffman.percorrer(tree);
            String codeHUF = huffman.encode(nodes, b);
            float porcentagemHUF = (100 * (codeHUF.length() / ((float) t * 8)));
            //=====================================FIM HUFFMAN================================================


            //=========================================RLE====================================================

            String codeRle = rle.encode(b);
            float porcentagemRle = (100 * (codeRle.length() / ((float) t * 8)));
            System.out.println(porcentagemRle);

            //=======================================FIM RLE==================================================


            if (porcentagemHUF < porcentagemRle) {
                contSaida++;
                printValue(codeHUF, arq, contSaida, porcentagemHUF, "HUF");
            } else if (porcentagemRle < porcentagemHUF) {
                contSaida++;
                printValue(codeRle, arq, contSaida, porcentagemRle, "RLE");
            } else {
                contSaida++;
                printValue(codeHUF, arq, contSaida, porcentagemHUF, "HUF");
                printValue(codeRle, arq, contSaida, porcentagemRle, "RLE");
            }


        }

        writer.append(arq);
        writer.close();
        br.close();

        System.out.println(System.currentTimeMillis() - millis);
    }

    private static Node[] getValores(int[] b) {
        int[] aux = new int[b.length];
        System.arraycopy(b, 0, aux, 0, aux.length);
        new CountingSort().sort(aux);

        int j = 0;

        Node[] node = new Node[aux.length];
        for (int i = 0; i < node.length; ++i) {
            node[i] = new Node(0, 0);
        }

        for (int i = 0; i < aux.length; i++) {
            node[j].valor = aux[i];

            if (i != aux.length - 1 && aux[i] == aux[i + 1]) {
                node[j].prio++;

            } else {
                node[j].prio++;
                j++;
            }
        }

        Node node1[] = new Node[j];

        System.arraycopy(node, 0, node1, 0, j);

        return node1;

    }

    static void printValue(String code, StringBuilder arq, int contSaida, float porcentagem, String tipo) {
        DecimalFormat df = new DecimalFormat("0.00");

        arq.append(String.valueOf(contSaida)).append(": [").append(tipo).append(" ").append(df.format(porcentagem)).append("%] 0x");
        for (int j = 0, k = 4, l = 1; k <= code.length(); j += 4, k += 4, l++) {
            arq.append(binaryToHex(code.substring(j, k)));
            if (l == 2 && k < code.length()) {
                l = 0;
                arq.append(" 0x");
            }

        }
        arq.append("\n");
    }

}