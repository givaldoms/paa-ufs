
import java.io.*;

public class givaldomarques_201420029045_criptografia {

    private static int keyCount = 0;
    private static long key;


    private static long pow(long b, long e, long p) {
        long r = b;
        for (int i = 0; i < e - 1; i++) {
            r = ((b * r) % p);
        }
        return r;
    }

    public static void main(String[] args) throws IOException {

        double t = System.currentTimeMillis();

        String nomeArq = args.length > 1 ? args[0] : "C:\\Users\\Givaldo Marques\\IdeaProjects\\projeto01-paa\\src\\entrada-criptografia.txt";
        String nomeSaida = args.length > 1 ? args[1] : "C:\\Users\\Givaldo Marques\\IdeaProjects\\projeto01-paa\\src\\saida-criptografia-long.txt";

        BufferedReader br = new BufferedReader(new FileReader(nomeArq));
        Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(nomeSaida), "utf-8"));

        //chaves privadas
        int a = Integer.parseInt(br.readLine().split(" ")[1]);//A 12
        int b = Integer.parseInt(br.readLine().split(" ")[1]);//B 45

        String[] aux = br.readLine().split(" ");

        //DH 101 16
        long p = Integer.parseInt(aux[1]);
        long g = Integer.parseInt(aux[2]);

        long A = pow(g, a, p);
        long B = pow(g, b, p);

        writer.append("A->B: ").append(String.valueOf(A)).append("\n");
        writer.append("B->A: ").append(String.valueOf(B)).append("\n");

        key = (pow(A, b, p));
        int n = Integer.parseInt(br.readLine());


        for (int i = 0; i < n; i++) {//cada fala
            String fala = br.readLine();
            if (i % 2 == 0) {
                writer.append("A->B: ");
                for (int j = 0; j < fala.length(); j++) {//para cada caractere da fala
                    writer.append(String.valueOf(fala.charAt(j) ^ G())).append(" ");
                }
            } else {

                writer.append("B->A: ");
                for (int j = 0; j < fala.length(); j++) {//para cada caractere da fala
                    writer.append(String.valueOf(fala.charAt(j) ^ G())).append(" ");
                }
            }

            writer.append("\n");
        }

        writer.close();
        br.close();

        System.out.println("tempo: " + (System.currentTimeMillis() - t));
    }


    private static long G() {
        int m = 1103515245;
        int d = 12345;

        if (keyCount == 0) {
            key = (m * key) + d;
        }


        long z = (key >> (keyCount * 8)) & 0xFF;
        keyCount = (keyCount + 1) % 4;

        return z;
    }

}
