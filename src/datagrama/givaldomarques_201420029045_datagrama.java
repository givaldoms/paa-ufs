package datagrama;

import java.io.*;

/**
 * Created by Givaldo Marques on 17/07/2017.
 */

class Datagrama {
    private int posicao;
    private int tam;
    private String valor;

    Datagrama(Datagrama d) {
        this.valor = d.valor;
        this.tam = d.tam;
        this.posicao = d.posicao;
    }

    Datagrama(String linha) {
        if (linha != null) {
            String[] aux = linha.split(" ", 3);
            this.valor = aux[2];
            this.posicao = Integer.parseInt(aux[0]);
            this.tam = Integer.parseInt(aux[1]);
            if (this.valor == null) {
                //   System.out.println(this.getPosicao());
            }


        }
    }

    String getValor() {
        return this.valor;
    }

    //void imprimeValor() {
    //  System.out.print(this.getValor() + " ");
    //}

    int getPosicao() {
        return posicao;
    }

    public static boolean validaEntrada(Datagrama[] d) {
        for (int i = 0; i < d.length; i++) {
            Datagrama d0 = d[i];
            if (d0.getValor() == null) {
                //System.out.print(d0.getPosicao() + " ");
                //System.out.print(d0.getValor());
                return false;
            }
        }
        return true;
    }

}

class HeapSort {

    void sort(Datagrama[] v) {
        int n = v.length;

        for (int i = n / 2 - 1; i >= 0; i--) {
            heapify(v, n, i);
        }

        for (int i = n - 1; i >= 0; i--) {
            trocar(v, 0, i);
            heapify(v, i, 0);
        }
    }

    void heapify(Datagrama[] v, int n, int i) {
        int p = i;
        int e = esquerdo(i);
        int d = direito(i);

        if (e < n && v[e].getPosicao() > v[p].getPosicao()) {
            p = e;
        }

        if (d < n && v[d].getPosicao() > v[p].getPosicao()) {
            p = d;
        }

        if (p != i) {
            trocar(v, p, i);
            heapify(v, p, n);
        }

    }

    private void trocar(Datagrama a[], int i, int j) {
        Datagrama temp = a[i];
        a[i] = a[j];
        a[j] = temp;
    }

    private int direito(int i) {
        return (2 * i) + 2;
    }

    private int esquerdo(int i) {
        return (2 * i) + 1;
    }
}

public class givaldomarques_201420029045_datagrama {

    public static void main(String[] args) throws IOException {

        HeapSort heapSort = new HeapSort();
        String nomeArq = args.length > 1 ? args[0] : "C:\\Users\\Givaldo Marques\\IdeaProjects\\projeto01-paa\\src\\entrada-datagrama.txt";
        BufferedReader br = new BufferedReader(new FileReader(nomeArq));

        String[] l = br.readLine().split(" ");//pegando os tamanhos
        int totalPacotes = Integer.parseInt(l[0]);
        int tamPacotes = Integer.parseInt(l[1]);

        Datagrama[] datagramas = new Datagrama[totalPacotes];
        Datagrama[] buffer = new Datagrama[totalPacotes];
        for (int i = 0; i < totalPacotes; i++) {
            String l1 = br.readLine();
            datagramas[i] = new Datagrama(l1);
        }

        String nomeSaida = args.length > 1 ? args[1] : "C:\\Users\\Givaldo Marques\\IdeaProjects\\projeto01-paa\\src\\saida-datagrama2.txt";
        Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(nomeSaida), "utf-8"));

        int next = 0;
        int cont = 0;
        int contImp = 0;
        int contBuffer = 0;
        int imprimiu = 0;

        writer.append(String.valueOf(contImp++)).append(": ");

        for (int i = 0; i < datagramas.length; i++, cont++) {
            Datagrama d = datagramas[i];

            if (cont >= tamPacotes && imprimiu == 1) {
                writer.append("\n").append(String.valueOf(contImp++)).append(": ");
                imprimiu = 0;
                cont = 0;
            } else if (cont >= tamPacotes) {
                cont = 0;
            }

            if (d.getPosicao() != next) {
                buffer[contBuffer++] = new Datagrama(d);
            } else {
                writer.append(d.getValor()).append(" ");
                next++;

                if (contBuffer == 0) {
                    imprimiu = 1;
                }

                Datagrama[] aux = new Datagrama[contBuffer];
                System.arraycopy(buffer, 0, aux, 0, contBuffer);
                heapSort.sort(aux);
                if (aux[0].getPosicao() == next) {
                    imprimiu = 1;
                    next++;
                    buffer = new Datagrama[totalPacotes];
                    contBuffer = 0;
                    for (Datagrama a : aux) {
                        writer.append(a.getValor()).append(" ");
                    }
                }
            }
        }

        writer.close();
        br.close();

    }


}
