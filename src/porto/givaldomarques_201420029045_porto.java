package porto;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;

/*
Projeto 01 de PAA - Prof Bruno Prado

PROBLEMA:
    .A empresa de automação portuária Poxim Tech está desenvolvendo um sistema para movimentação
automatizada dos contêineres de carga de origem internacional no Porto de Sergipe para maximizar a
eficiência da fiscalização aduaneira
   .Todos os contêineres possuem um cadastro eletrônico contendo informações sobre o código do contêiner,
o CNPJ da empresa importadora e o peso líquido em quilos da carga
   .A inspeção dos contêineres é realizada sempre que existe alguma divergência entre as informações
cadastradas, como o CNPJ informado ou a diferença percentual maior do que 10% no peso líquido

    .Na triagem dos contêineres são fiscalizados os contêineres com a seguinte ordem de prioridade:
        1. Divergência de CNPJ
        2. Maior diferença percentual de peso líquido

SOLUÇÃO:
    . Salva o arquivo de entrada em três arrays de Containers (inclusive a ordem de chegada)
        - containerE: Contém todos os Containers que chegaram no porto
        - containerS: Contém os containers selecionados para a inspeção
        - containerAux: containerE ordenado por código
    . Mapeia a ordem de chegada de containerS usando auxContainer
        - int p = main.binarySearch(containerAux, container.codigo);
        - containerS.posicao = containerAux[p].posicao;

    . Ordena o containerS por ordem de chegada
    . Faz a triagem buscando por CNPJ diferente
    . Faz a triagem buscando por CNPJ igual e diferença de peso maior que 10% e menor que 10%

*/

/**
 * Created by Givaldo Marques on 28/06/2017.
 */


/*
 * Classe porto.Container: Armazenar as informações do arquivo
 */
class Container {

    String codigo;
    String cnpj;
    int peso;
    double diferencaPeso;

    //Salva a ordem de chegada do container
    int posicao;

    public Container(String codigo, String cnpj, int peso, int posicao) {
        this.codigo = codigo;
        this.cnpj = cnpj;
        this.peso = peso;
        this.posicao = posicao;
        this.diferencaPeso = 0;
    }

    public Container(String codigo, String cnpj, int peso, double diferencaPeso, int posicao) {
        this.codigo = codigo;
        this.cnpj = cnpj;
        this.peso = peso;
        this.diferencaPeso = diferencaPeso;
        this.posicao = posicao;
    }

    /*
          Pega informações assim: DKSNIWSPOLD 32.899.211/2482-11 48442
          e salva retorna um container
          @param linha informação que vem do arquivo
          @param posicao número da linha no arquivo
          @return porto.Container
         */
    static Container parseContainer(String linha, int posicao) {
        String[] l = linha.split(" ");
        return new Container(l[0], l[1], Integer.parseInt(l[2]), posicao);
    }

}

/*
 * Ordena um array de objetos porto.Container usando Merge sortNormal
 */
@SuppressWarnings("Duplicates")
class ContainerMergeSorte {

    private Container[] container;
    private Container[] tempContainer;
    private int campoSort;

    void sort(Container[] e, int campoSort) {
        this.campoSort = campoSort;
        this.container = e;
        int tamanho = e.length;
        this.tempContainer = new Container[tamanho];
        doMergeSort(0, tamanho - 1);
    }

    private void doMergeSort(int inicio, int fim) {
        if (inicio < fim) {
            int meio = inicio + (fim - inicio) / 2;
            doMergeSort(inicio, meio);
            doMergeSort(meio + 1, fim);
            if (campoSort == 0) {
                mergePartsByCodigo(inicio, meio, fim);
            } else if (campoSort == 1) {
                mergePartsByPosicao(inicio, meio, fim);
            } else {
                mergePartsByPeso(inicio, meio, fim);
            }
        }
    }

    private void mergePartsByCodigo(int inicio, int meio, int fim) {
        System.arraycopy(container, inicio, tempContainer, inicio, fim + 1 - inicio);

        int i = inicio, j = meio + 1, k = inicio;
        while (i <= meio && j <= fim) {
            if (tempContainer[i].codigo.compareTo(tempContainer[j].codigo) <= 0) {
                container[k] = tempContainer[i++];
            } else {
                container[k] = tempContainer[j++];
            }
            k++;
        }

        while (i <= meio) {
            container[k++] = tempContainer[i++];
        }

    }

    private void mergePartsByPeso(int inicio, int meio, int fim) {
        System.arraycopy(container, inicio, tempContainer, inicio, fim + 1 - inicio);

        int i = inicio, j = meio + 1, k = inicio;
        while (i <= meio && j <= fim) {
            if (tempContainer[i].diferencaPeso > tempContainer[j].diferencaPeso) {
                container[k] = tempContainer[i++];
            } else {
                container[k] = tempContainer[j++];
            }
            k++;
        }

        while (i <= meio) {
            container[k++] = tempContainer[i++];
        }

    }

    private void mergePartsByPosicao(int inicio, int meio, int fim) {
        System.arraycopy(container, inicio, tempContainer, inicio, fim + 1 - inicio);

        int i = inicio, j = meio + 1, k = inicio;
        while (i <= meio && j <= fim) {
            if (tempContainer[i].posicao <= tempContainer[j].posicao) {
                container[k] = tempContainer[i++];
            } else {
                container[k] = tempContainer[j++];
            }
            k++;
        }

        while (i <= meio) {
            container[k++] = tempContainer[i++];
        }

    }

}

public class givaldomarques_201420029045_porto {

    private int binarySearch(Container[] c, String chave) {
        int tam = c.length;
        int inicio = 0, fim = tam - 1, meio;

        while (inicio <= fim) {
            meio = (fim + inicio) / 2;
            if (c[meio].codigo.compareTo(chave) == 0) {
                return meio;
            }

            if (c[meio].codigo.compareTo(chave) > 0) {
                fim = meio - 1;
            } else {
                inicio = meio + 1;
            }
        }

        return -1;
    }

    @SuppressWarnings("unused")
    private void imprime(Container[] containers) {
        for (Container c : containers) {
            System.out.print(c.codigo + " ");
            System.out.print(c.cnpj + " ");
            System.out.print(c.peso + " ");
            System.out.println(c.posicao + " ");
        }
    }

    public static void main(String[] args) {

        long tempoExe = System.currentTimeMillis();

        Container[] containerE = new Container[0];
        Container[] containerS = new Container[0];

        givaldomarques_201420029045_porto main = new givaldomarques_201420029045_porto();
        ContainerMergeSorte cmSort = new ContainerMergeSorte();

        String nomeArq = args.length > 1 ? args[0] : "C:\\Users\\Givaldo Marques\\IdeaProjects\\projeto01-paa\\src\\entrada-porto.txt";

        try (BufferedReader br = new BufferedReader(new FileReader(nomeArq))) {
            int qContainer1 = Integer.parseInt(br.readLine());

            containerE = new Container[qContainer1];
            for (int i = 0; i < qContainer1; i++) {
                containerE[i] = Container.parseContainer(br.readLine(), i);
            }

            int qContainer2 = Integer.parseInt(br.readLine());
            containerS = new Container[qContainer2];
            for (int i = 0; i < qContainer2; i++) {
                containerS[i] = Container.parseContainer(br.readLine(), i);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        Container[] containerAux = new Container[containerE.length];

        System.arraycopy(containerE, 0, containerAux, 0, containerE.length);

        cmSort.sort(containerAux, 0);

        for (Container container : containerS) {//busca no array ordenado
            int p = main.binarySearch(containerAux, container.codigo);
            container.posicao = containerAux[p].posicao;
        }

        cmSort.sort(containerS, 1);

        Writer writer = null;
        String nomeSaida = args.length > 1 ? args[1] : "C:\\Users\\Givaldo Marques\\IdeaProjects\\projeto01-paa\\src\\saida-porto.txt";
        try {
            writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(nomeSaida), "utf-8"));

            for (Container c : containerS) {
                if (!c.cnpj.equals(containerE[c.posicao].cnpj)) {
                    writer.append(c.codigo).append(": ").append(containerE[c.posicao].cnpj).append("<->").append(c.cnpj).append("\n");
                }
            }

            Container[] tt = new Container[containerAux.length - 1];
            int cTt = 0;
            for (Container c : containerS) {
                int pesoContainer = containerE[c.posicao].peso;
                int diferenca = Math.round(c.peso - pesoContainer);
                double porcentagem = Math.round((diferenca / (double) pesoContainer) * 100);

                if ((porcentagem > 10 || porcentagem < -10) && c.cnpj.equals(containerE[c.posicao].cnpj)) {
                    //writer.append(c.codigo).append(": ").append(String.valueOf(Math.abs(diferenca))).append("kg (").append(String.valueOf((int) Math.abs(porcentagem))).append("%)\n");
                    tt[cTt++] = new Container(c.codigo, c.cnpj, diferenca, Math.abs(porcentagem), c.posicao);
                }

            }

            Container[] ttAux = new Container[cTt-1];

            System.arraycopy(tt, 0, ttAux, 0, cTt-1);

            cmSort.sort(ttAux, 2);

            for (int i = 0; i < cTt-1; i++) {
                Container c = ttAux[i];
                writer.append(c.codigo).append(": ").append(String.valueOf(Math.abs(c.peso))).append("kg (").append(String.valueOf(Math.abs(Math.round(c.diferencaPeso)))).append("%)\n");
            }
        } catch (IOException ignore) {
        } finally {
            try {
                if (writer != null) writer.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        System.out.println("Tempo: " + (System.currentTimeMillis() - tempoExe) + "ms");

    }
}