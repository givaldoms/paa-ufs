package quicksort;

import java.io.*;

/**
 * Created by Givaldo Marques on 04/07/2017.
 */

class Saida {
    private int valor;
    private String tipo;

    Saida(int valor, String tipo) {
        this.valor = valor;
        this.tipo = tipo;
    }

    Saida() {
    }

    @Override
    public String toString() {
        return tipo + "(" + valor + ")";
    }

    void imprime() {
        System.out.print(this.toString() + " ");
    }

    void sort(Saida[] a) {
        if (a == null || a.length == 0) return;
        quickSort(a, 0, a.length - 1);
    }

    private void quickSort(Saida[] a, int inicio, int fim) {
        if (inicio < fim) {
            int pivo = particionar(a, inicio, fim);
            quickSort(a, inicio, pivo - 1);
            quickSort(a, pivo + 1, fim);
        }
    }

    private int particionar(Saida[] a, int inicio, int fim) {
        int pivo = a[fim].valor;
        int i = inicio - 1, j;
        for (j = inicio; j < fim; j++) {
            if (a[j].valor <= pivo) {
                i = i + 1;
                exchangeNumbers(a, i, j);
            }
        }
        exchangeNumbers(a, (i + 1), fim);
        return (i + 1);
    }

    private void exchangeNumbers(Saida a[], int i, int j) {
        Saida temp = a[i];
        a[i] = a[j];
        a[j] = temp;
    }


}

@SuppressWarnings("Duplicates")
class QuickSort {

    int nTrocas = 0;

    void sortNormal(int[] a) {
        nTrocas = 0;
        if (a == null || a.length == 0) return;
        quickSortNormal(a, 0, a.length - 1);
    }

    private void quickSortNormal(int[] a, int inicio, int fim) {
        nTrocas++;
        if (inicio < fim) {
            int pivo = particionarNormal(a, inicio, fim);
            quickSortNormal(a, inicio, pivo - 1);
            quickSortNormal(a, pivo + 1, fim);
        }
    }

    private int particionarNormal(int[] a, int inicio, int fim) {
        int pivo = a[fim];
        int i = inicio - 1, j;
        for (j = inicio; j < fim; j++) {
            if (a[j] <= pivo) {
                i = i + 1;
                exchangeNumbers(a, i, j);
            }
        }
        exchangeNumbers(a, (i + 1), fim);
        return (i + 1);
    }

    void sortMediana(int[] a) {
        nTrocas = 0;
        if (a == null || a.length == 0) return;
        quickSortMediana(a, 0, a.length - 1);
    }

    private void quickSortMediana(int[] a, int inicio, int fim) {
        nTrocas++;
        if (inicio < fim) {
            int pivo = particionarMediana(a, inicio, fim);
            quickSortMediana(a, inicio, pivo - 1);
            quickSortMediana(a, pivo + 1, fim);
        }
    }

    private int particionarMediana(int[] a, int inicio, int fim) {
        int n = fim - inicio + 1;
        exchangeNumbers(a, getMediana(a, n, inicio), fim);
        int pivo = a[fim];
        int i = inicio - 1, j;
        for (j = inicio; j < fim; j++) {
            if (a[j] <= pivo) {
                i = i + 1;
                exchangeNumbers(a, i, j);
            }
        }
        exchangeNumbers(a, (i + 1), fim);
        return (i + 1);
    }

    void sortAleatorio(int[] a) {
        nTrocas = 0;
        if (a == null || a.length == 0) return;
        quickSortAleatorio(a, 0, a.length - 1);
    }

    private void quickSortAleatorio(int[] a, int inicio, int fim) {
        nTrocas++;
        if (inicio < fim) {
            int pivo = particionarAleatorio(a, inicio, fim);
            quickSortAleatorio(a, inicio, pivo - 1);
            quickSortAleatorio(a, pivo + 1, fim);
        }
    }

    private int particionarAleatorio(int[] a, int inicio, int fim) {
        int aux = inicio + (Math.abs(a[inicio]) % (fim - inicio + 1));
        exchangeNumbers(a, aux, fim);
        int pivo = a[fim];

        int i = inicio - 1, j;
        for (j = inicio; j < fim; j++) {
            if (a[j] <= pivo) {
                i = i + 1;
                exchangeNumbers(a, i, j);
            }
        }

        exchangeNumbers(a, (i + 1), fim);
        return (i + 1);
    }

    private void exchangeNumbers(int a[], int i, int j) {
        nTrocas++;
        int temp = a[i];
        a[i] = a[j];
        a[j] = temp;
    }

    private int getMediana(int[] a, int n, int inicio) {
        int v1 = a[inicio + (n / 4)];
        int v2 = a[inicio + (n / 2)];
        int v3 = a[inicio + ((3 * n) / 4)];

        if (v1 >= v2 && v1 <= v3 || v1 >= v3 && v1 <= v2) {
            return inicio + (n / 4);
        } else if (v2 >= v1 && v2 <= v3 || v2 >= v3 && v2 <= v1) {
            return inicio + (n / 2);
        } else {
            return inicio + ((3 * n) / 4);
        }

    }

}

@SuppressWarnings("Duplicates")
class Hoare {
    int nTrocas = 0;

    void sortNormal(int[] a) {
        nTrocas = 0;
        if (a == null || a.length == 0) return;
        quickSortNormal(a, 0, a.length - 1);
    }

    private void quickSortNormal(int[] a, int inicio, int fim) {
        nTrocas++;
        if (inicio < fim) {
            int pivo = particionarNormal(a, inicio, fim);
            quickSortNormal(a, inicio, pivo);
            quickSortNormal(a, pivo + 1, fim);
        }
    }

    private int particionarNormal(int[] a, int inicio, int fim) {
        int pivo = a[inicio];
        int i = inicio, j = fim;

        while (i < j) {
            while (j > i && a[j] >= pivo) j--;
            while (i < j && a[i] < pivo) i++;
            if (i < j) exchangeNumbers(a, i, j);
        }

        return j;
    }

    void sortMediana(int[] a) {
        nTrocas = 0;
        if (a == null || a.length == 0) return;
        quickSortMediana(a, 0, a.length - 1);
    }

    private void quickSortMediana(int[] a, int inicio, int fim) {
        nTrocas++;
        if (inicio < fim) {
            int pivo = particionarMediana(a, inicio, fim);
            quickSortMediana(a, inicio, pivo);
            quickSortMediana(a, pivo + 1, fim);
        }
    }

    private int particionarMediana(int[] a, int inicio, int fim) {
        int n = fim - inicio + 1;
        exchangeNumbers(a, getMediana(a, n, inicio), inicio);
        int pivo = a[inicio];
        int i = inicio, j = fim;

        while (i < j) {
            while (j > i && a[j] >= pivo) j--;
            while (i < j && a[i] < pivo) i++;
            if (i < j) exchangeNumbers(a, i, j);
        }

        return j;
    }

    void sortAleatorio(int[] a) {
        nTrocas = 0;
        if (a == null || a.length == 0) return;
        quickSortAleatorio(a, 0, a.length - 1);
    }

    private void quickSortAleatorio(int[] a, int inicio, int fim) {
        nTrocas++;
        if (inicio < fim) {
            int pivo = particionarAleatorio(a, inicio, fim);
            quickSortAleatorio(a, inicio, pivo);
            quickSortAleatorio(a, pivo + 1, fim);
        }
    }

    private int particionarAleatorio(int[] a, int inicio, int fim) {
        int aux = inicio + (Math.abs(a[inicio]) % (fim - inicio + 1));
        exchangeNumbers(a, aux, inicio);
        int pivo = a[inicio];
        int i = inicio, j = fim;

        while (i < j) {
            while (j > i && a[j] >= pivo) j--;
            while (i < j && a[i] < pivo) i++;
            if (i < j) exchangeNumbers(a, i, j);
        }

        return j;
    }

    private void exchangeNumbers(int a[], int i, int j) {
        nTrocas++;
        int temp = a[i];
        a[i] = a[j];
        a[j] = temp;
    }

    private int getMediana(int[] a, int n, int inicio) {
        int v1 = a[inicio + (n / 4)];
        int v2 = a[inicio + (n / 2)];
        int v3 = a[inicio + ((3 * n) / 4)];

        if (v1 >= v2 && v1 <= v3 || v1 >= v3 && v1 <= v2) {
            return inicio + (n / 4);
        } else if (v2 >= v1 && v2 <= v3 || v2 >= v3 && v2 <= v1) {
            return inicio + (n / 2);
        } else {
            return inicio + ((3 * n) / 4);
        }

    }

}

public class givaldomarques_201420029045_quicksort {

    public static void main(String[] args) throws IOException {
        String nomeArq = args.length > 1 ? args[0] : "C:\\Users\\Givaldo Marques\\IdeaProjects\\projeto01-paa\\src\\entrada_quick.txt";
        String nomeSaida = args.length > 1 ? args[1] : "C:\\Users\\Givaldo Marques\\IdeaProjects\\projeto01-paa\\src\\saida_quick.txt";

        Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(nomeSaida), "utf-8"));

        int[] valores;
        Saida[] saida;
        BufferedReader br = new BufferedReader(new FileReader(nomeArq));
        int quantidadeVet = Integer.parseInt(br.readLine());

        for (int i = 0; i < quantidadeVet; i++) {
            int saidaCont = 0;

            int tamVet = Integer.parseInt(br.readLine());
            valores = new int[tamVet];
            saida = new Saida[7];
            saida[saidaCont++] = new Saida(tamVet, "N");
            String[] linha = br.readLine().split(" ");
            for (int j = 0; j < tamVet; j++) {
                valores[j] = Integer.parseInt(linha[j]);
            }

            QuickSort q = new QuickSort();
            Hoare h = new Hoare();

            int[] aux = new int[valores.length];

            System.arraycopy(valores, 0, aux, 0, valores.length);
            q.sortNormal(aux);
            saida[saidaCont++] = new Saida(q.nTrocas, "PP");

            System.arraycopy(valores, 0, aux, 0, valores.length);
            q.sortAleatorio(aux);
            saida[saidaCont++] = new Saida(q.nTrocas, "PA");

            System.arraycopy(valores, 0, aux, 0, valores.length);
            q.sortMediana(aux);
            saida[saidaCont++] = new Saida(q.nTrocas, "PM");

            System.arraycopy(valores, 0, aux, 0, valores.length);
            h.sortNormal(aux);
            saida[saidaCont++] = new Saida(h.nTrocas, "HP");

            System.arraycopy(valores, 0, aux, 0, valores.length);
            h.sortAleatorio(aux);
            saida[saidaCont++] = new Saida(h.nTrocas, "HA");

            System.arraycopy(valores, 0, aux, 0, valores.length);
            h.sortMediana(aux);
            saida[saidaCont++] = new Saida(h.nTrocas, "HM");


            new Saida().sort(saida);
            writer.append(String.valueOf(i)).append(": ");
           // System.out.print(i + ": ");
            for (int c = 0; c < saidaCont; c++) {
               // System.out.print(saida[c].toString());
                writer.append(saida[c].toString()).append(" ");
            }
            writer.append("\n");
            //System.out.println();
        }

        writer.close();
        br.close();
    }

}
