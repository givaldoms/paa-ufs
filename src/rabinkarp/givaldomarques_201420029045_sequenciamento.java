package rabinkarp;

import java.io.*;

class Doenca {

    String[] genes;
    int chandeOcorrencia;
    private String codigo;

    Doenca(String codigo, String genes) {
        this.codigo = codigo;
        this.genes = genes.split(" ");
    }

    @Override
    public String toString() {
        return codigo + ": " + chandeOcorrencia + "%";
    }
}

class Mergesort {
    private Doenca[] numbers;
    private Doenca[] helper;

    void sort(Doenca[] values) {
        this.numbers = values;
        int number = values.length;
        this.helper = new Doenca[number];
        mergesort(0, number - 1);
    }

    private void mergesort(int low, int high) {
        if (low < high) {
            int middle = low + (high - low) / 2;
            mergesort(low, middle);
            mergesort(middle + 1, high);
            merge(low, middle, high);
        }
    }

    private void merge(int low, int middle, int high) {

        System.arraycopy(numbers, low, helper, low, high + 1 - low);

        int i = low;
        int j = middle + 1;
        int k = low;
        while (i <= middle && j <= high) {
            if (helper[i].chandeOcorrencia >= helper[j].chandeOcorrencia) {
                numbers[k] = helper[i];
                i++;

            } else {
                numbers[k] = helper[j];
                j++;
            }
            k++;
        }

        while (i <= middle) {
            numbers[k] = helper[i];
            k++;
            i++;
        }

    }
}

class givaldomarques_201420029045_sequenciamento {

    public static void main(String[] args) throws IOException {

        //long timer = System.currentTimeMillis();
        String nomeArq = args.length > 1 ? args[0] : "C:\\Users\\Givaldo Marques\\IdeaProjects\\projeto01-paa\\src\\rabinkarp\\entrada-rk.txt";
        String nomeSaida = args.length > 1 ? args[1] : "C:\\Users\\Givaldo Marques\\IdeaProjects\\projeto01-paa\\src\\rabinkarp\\saida-rk.txt";

        givaldomarques_201420029045_sequenciamento rabinKarp = new givaldomarques_201420029045_sequenciamento();

        BufferedReader br = new BufferedReader(new FileReader(nomeArq));
        int tamSubcadeia = Integer.parseInt(br.readLine());

        String T = br.readLine();

        //int[] pos = new int[T.length()];
        //int[] tab = new int[tamSubcadeia];

        int nDoencas = Integer.parseInt(br.readLine());
        Doenca[] doencas = new Doenca[nDoencas];

        //System.out.println("Antes a leituta: " + (System.currentTimeMillis() - timer) + "ms");
        for (int i = 0; i < nDoencas; i++) {
            String[] s = br.readLine().split(" ", 3);//exemplo de linha: ABCDE 3 AAA AAT AAAG
            doencas[i] = new Doenca(s[0], s[2]);
        }
        //System.out.println("Após a leituta: " + (System.currentTimeMillis() - timer) + "ms");

        for (Doenca doenca : doencas) {
            int qGenesAtivos = 0;
            String[] ps = doenca.genes;

            for (String p0 : ps) {//para cada gene
                int a = 0;

                String tAux = T.substring(a, T.length());
                int qAchados = p0.length() - rabinKarp.busca_kmp(tamSubcadeia, tAux, p0);//quantos sobraram

                // System.out.print(qAchados + " ");
                //verifica se o gene está ativo
                if (qAchados >= p0.length() * 0.9) {
                    qGenesAtivos++;
                }
            }

            // System.out.println("genes ativos" + qGenesAtivos);
            //System.out.println(doenca.codigo +": "+ Math.round((qGenesAtivos * 100) / (float)ps.length) + "%");
            doenca.chandeOcorrencia = Math.round((qGenesAtivos * 100) / (float) ps.length);
            // System.out.println("\n======");

        }

        //System.out.println("Antes de ordenar: " + (System.currentTimeMillis() - timer) + "ms");
        new Mergesort().sort(doencas);

        //System.out.println("Antes de imprimir: " + (System.currentTimeMillis() - timer) + "ms");
        Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(nomeSaida)));

        for (Doenca d : doencas) {
            writer.append(d.toString()).append("\n");
        }

        writer.close();
        br.close();

        //System.out.println("Fim: " + (System.currentTimeMillis() - timer) + "ms");
    }

    private void calcular_tabela(int tab[], String P) {
        int i, m = P.length();
        int j = -1;
        inicializar(tab, m);

        for (i = 1; i < m; i++) {
            while (j >= 0 && P.charAt(j + 1) != P.charAt(i)) {
                j = tab[j];
            }

            if (P.charAt(j + 1) == P.charAt(i)) {
                j++;
            }
            tab[i] = j;
        }

    }

    private int busca_kmp(int subcadeia, String T, String P) {
        int i, n = T.length();
        int m = P.length();
        int j = -1;

        int[] tab = new int[m];

        calcular_tabela(tab, P);

        for (i = 0; i < n; i++) {
            while ((j >= 0) && (P.charAt(j + 1) != T.charAt(i))) {

                if (j >= subcadeia - 1) {
                    P = P.substring(j + 1, P.length());
                    calcular_tabela(tab, P);
                    j = -1;
                    m = P.length();
                    if (P.length() < subcadeia) return P.length();
                    break;
                }

                j = tab[j];
            }

            if (P.charAt(j + 1) == T.charAt(i)) {
                j++;
            }

            if (j == m - 1) {//achou toda a palavra
                return 0;

            }

        }
        return P.length();
    }

    private void inicializar(int tab[], int m) {
        for (int i = 0; i < m; i++) {
            tab[i] = -1;
        }
    }

}
